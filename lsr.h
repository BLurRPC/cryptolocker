#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "crypto.h"
#include <unistd.h>

void ls(char* chemin, unsigned char *ckey, unsigned char *ivec, int shouldEncrypt);//Fonction qui chiffre tout les fichiers d'un repertoire
void lsr(char* chemin, unsigned char *ckey, unsigned char *ivec, int shouldEncrypt);//Fonction qui parcourt tout les repertoires
