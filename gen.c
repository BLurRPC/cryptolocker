#include "gen.h"

unsigned char* genKeyAndIv(int buffer_size) {
    unsigned char *gen = malloc(sizeof(unsigned char)*buffer_size);
    if (!RAND_bytes(gen, buffer_size)) { //Generate random key
        printf("Key generating failed\n");
        exit(EXIT_FAILURE);
    }
    for(int i=0;i<16;i++) {
			printf("%i ",gen[i]);
		}
		printf("\n");
    return gen;
}