#include "crypto.h"

/*
 Encrypt or decrypt, depending on flag 'should_encrypt'
 */
void en_de_crypt(int should_encrypt, FILE *ifp, FILE *ofp, unsigned char *ckey, unsigned char *ivec) {

    const unsigned BUFSIZE=8;
    unsigned char *read_buf = malloc(BUFSIZE);
    unsigned char *cipher_buf;
    unsigned blocksize;
    int out_len;
    int numRead = 0;
    EVP_CIPHER_CTX ctx;

	if(1!=EVP_CipherInit(&ctx, EVP_aes_256_cbc(), ckey, ivec, should_encrypt))
		handleErrors();
	blocksize = EVP_CIPHER_CTX_block_size(&ctx);
	cipher_buf = malloc(BUFSIZE + blocksize);

    while ((numRead = fread(read_buf, sizeof(unsigned char), BUFSIZE, ifp))!=EOF) {

        // Read in data in blocks until EOF. Update the ciphering with each read.

		if(1!=EVP_CipherUpdate(&ctx, cipher_buf, &out_len, read_buf, numRead))
			handleErrors();
		fwrite(cipher_buf, sizeof(unsigned char), out_len, ofp);
		if (numRead < BUFSIZE) { // EOF
			break;
        }
    }

    // Now cipher the final block and write it out.

    EVP_CipherFinal(&ctx, cipher_buf, &out_len);
    fwrite(cipher_buf, sizeof(unsigned char), out_len, ofp);

    // Free memory

    free(cipher_buf);
    free(read_buf);
}

void handleErrors(void) {

	ERR_print_errors_fp(stderr);
	abort();
}
