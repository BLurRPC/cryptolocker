#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define BUFFER_SIZE 16

#define PORT 30000 /*PORT utilisé*/

void doProcessing(int s2, struct sockaddr_in remote) {

	int i;
	int n=0; //Size of what we have received
	char *argument1 = "do";
	unsigned char buffer[BUFFER_SIZE];
	char *signal = malloc(sizeof(char)*2); //Crypt or Decrypt
	FILE* f;
	char FileName[INET_ADDRSTRLEN];
	strcpy(FileName,inet_ntoa(remote.sin_addr)); //Stocking the client IP address
	strcat(FileName,".txt"); //Add the .txt extension

	printf("Connected 2\n");

	//Chiffrement ou dechiffrement ?
	if((n=recv(s2, signal, 2, 0))<=0)
	{
		printf("erreur argument1\n");
		exit(errno);
	}
	printf("%s\n",signal);
	//if "do"
	if(strcmp(argument1,signal)==0) {

		if((f = fopen(FileName,"rb"))!=NULL) {//If the file already exists, then error

			printf("File %s already exists\n",FileName);
			char *check="error";
			if(write(s2, check, 5)==0) //We send an error to the client
			{
				perror("sendCommande");
				exit(EXIT_FAILURE);
			}

				shutdown(s2,2); //On ferme la connection dans les 2 sens
			}
		else {

			char *checkGood="blurr"; //Means nothing
			if(write(s2, checkGood, 5)==0) //Send ok to continue
			{
				perror("sendCommande");
				exit(EXIT_FAILURE);
			}

			f = fopen(FileName,"wb"); //Write mode

			n=0; //Size of what we have received
			if((n=recv(s2, buffer, BUFFER_SIZE, 0))<0)
			{
				printf("erreur key buffer");
				exit(errno);
			}
			//Write the key
			for(i=0;i<16;i++) {
				//printf("%i ", buffer[i]);
				fwrite(&buffer[i], sizeof(unsigned char), 1,f);
			}
			//printf("\n");
			memset(buffer,0,BUFFER_SIZE);
			n=0; //Size of what we have received
			if((n=recv(s2, buffer, BUFFER_SIZE, 0))<0)
			{
				printf("erreur iv buffer");
				exit(errno);
			}
			//Write the iv
			for(i=0;i<16;i++) {
				//printf("%i ", buffer[i]);
				fwrite(&buffer[i], sizeof(unsigned char), 1,f);
			}
			fclose(f);
		}
	}
	//if "undo"
	else{
		
		if((f = fopen(FileName,"rb"))==NULL) //Read mode
		{
			perror("Error ClientFile");
			shutdown(s2,2); //On ferme la connection dans les 2 sens
		}
		else {
			//Get the key
			for(i=0;i<16;i++){
				fread(&buffer[i], sizeof(unsigned char), 1, f);
				//printf("%i ", buffer[i]);
			}
			//printf("\n");

			//send the key
			if(write(s2, buffer, BUFFER_SIZE)<0)
			{
				perror("sendCommande");
				exit(1);
			}

			memset(buffer,0,BUFFER_SIZE); //set the buffer to 0
			//Get the iv
			for(i=0;i<16;i++){
				fread(&buffer[i], sizeof(unsigned char), 1, f);
				//printf("%i ", buffer[i]);
			}
			//printf("\n");
			fclose(f);
			
			//send the iv
			if(write(s2, buffer, BUFFER_SIZE)<0)
			{
				perror("sendCommande");
				exit(1);
			}
		}
	}
	free(signal);
}

int main(int argc, char *argv[])
{
	int s, s2, t, pid;
	int yes=1;
	struct sockaddr_in local = {0};
	struct sockaddr_in remote = {0};	

	/*Creation socket*/
	if((s=socket(AF_INET, SOCK_STREAM, 0))==-1)
		{
			perror("socket");
			exit(1);
		}

	local.sin_addr.s_addr = htonl(INADDR_ANY); //Nous sommes un serveur, nous acceptons n'importe quelle addresse
	local.sin_family = AF_INET;
	local.sin_port = htons(PORT);

	//Reuse address		
	if(setsockopt(s,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int))==-1)
	{
		perror("setsockopt");
		exit(1);
	}		
	
	if (bind(s, (struct sockaddr *)&local, sizeof(local)) == -1)
	{
			perror("bind");
		exit(1);
	}

	//On ecoute sur le port, 5 machines max à la fois
	if(listen(s, 5) == -1)
	{
		perror("listen");
		exit(1);
	}

	while(1) {

		printf("waiting for connection\n");
		t=sizeof(remote);
		if ((s2 = accept(s, (struct sockaddr *)&remote, (socklen_t*)&t)) == -1)
		{	
			exit(1);
		}
		//Create Child process
		pid=fork();

		if (pid < 0) {
			perror("ERROR on fork");
			exit(EXIT_FAILURE);
      	}
      	else if(pid == 0) {
			// This is the client process
			close(s);
			doProcessing(s2, remote);
			exit(EXIT_SUCCESS);
      	}
      	else {
        	close(s2);
      	}
	}
	exit(EXIT_SUCCESS);
}
