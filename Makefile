all: main.o crypto.o lsr.o gen.o
	gcc -Wall -o main main.o crypto.o lsr.o gen.o -lcrypto
	gcc -Wall -o server server.c

crypto.o: crypto.c
	gcc -Wall -o crypto.o -c crypto.c -lcrypto

gen.o: gen.c
	gcc -Wall -o gen.o -c gen.c -lcrypto

lsr.o: lsr.c
	gcc -Wall -o lsr.o -c lsr.c

main.o: main.c
	gcc -Wall -o main.o -c main.c -lcrypto

clean:
	rm -rf *.o
rmproper: clean
	rm -rf main server 127.0.0.1.txt
